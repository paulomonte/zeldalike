﻿using System.Collections.Generic;
using UnityEngine;

public class BaseManager<T> : MonoBehaviour
{
    protected List<T> items = new List<T>();
    public List<T> Items { get => items; }

    public virtual void RegisterItem(T item)
    {
        if (items.Contains(item))
            return;

        items.Add(item);
    }

    public virtual void DeregisterItem(T item)
    {
        if(items.Contains(item))
        {
            items.Remove(item);
        }
    }
}
