using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenu : EndGameMenu
{
    private void Start()
    {
        GameController.instance.RegisterPauseMenu(this);
        gameObject.SetActive(false);
    }

    public void UnpauseGame()
    {
        GameController.instance.AlternatePauseState();
    }

    private void OnDestroy()
    {
        GameController.instance.DeregisterPauseMenu();
    }
}
