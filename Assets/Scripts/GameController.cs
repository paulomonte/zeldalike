using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    public static GameController instance;
    public bool gamePaused { get; private set; } = false;

    [SerializeField] private PauseMenu pauseMenu;
    [SerializeField] private LoseGameCanvas loseGameCanvas;
    private bool gameIsEnding;

    public event Action OnLoseGame;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }

    private void Start()
    {
        SceneManager.sceneLoaded += SceneManager_sceneLoaded;
        SceneManager.sceneUnloaded += SceneManager_sceneUnloaded;
    }

    private void SceneManager_sceneUnloaded(Scene arg0)
    {
        OnLoseGame = null;
    }

    private void SceneManager_sceneLoaded(Scene arg0, LoadSceneMode arg1)
    {
        gameIsEnding = false;
        UnpauseGame();
        AudioSystem.instance.Restart();
    }

    #region Pause System

    public void RegisterPauseMenu(PauseMenu menu)
    {
        pauseMenu = menu;
    }

    public void DeregisterPauseMenu()
    {
        pauseMenu = null;
    }

    public void AlternatePauseState()
    {
        if (gamePaused)
            UnpauseGame();
        else
            PauseGame();
    }

    private void PauseGame()
    {
        gamePaused = true;
        pauseMenu?.gameObject.SetActive(true);
        Time.timeScale = 0;
    }

    private void UnpauseGame()
    {
        gamePaused = false;
        pauseMenu?.gameObject.SetActive(false);
        Time.timeScale = 1;
    }

    #endregion

    #region Lose Game

    public void RegisterLoseCanvas(LoseGameCanvas canvas)
    {
        loseGameCanvas = canvas;
        loseGameCanvas.OnEndCanvasDuration += LoadPuzzleScene;
    }

    public void DeregisterLoseCanvas()
    {
        loseGameCanvas = null;
    }

    public void LoseGame()
    {
        if (!gameIsEnding)
        {
            gameIsEnding = true;
            StartCoroutine(LoseGame_Routine());
        }
    }

    private IEnumerator LoseGame_Routine()
    {
        OnLoseGame?.Invoke();

        if (loseGameCanvas)
        {
            loseGameCanvas.gameObject.SetActive(true);
            yield return loseGameCanvas.LosingGame_Routine();
        }
    }

    private void LoadPuzzleScene()
    {
        SceneManager.LoadScene("Puzzle");
    }

    #endregion

    #region Reach Goal

    public void OnReachGoal(string nextLevel)
    {
        SceneManager.LoadScene(nextLevel);
    }

    #endregion
}
