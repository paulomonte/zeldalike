using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Used to play audio on disable objects
/// </summary>
public class AudioSystem : MonoBehaviour
{
    public static AudioSystem instance;

    private List<AudioObject> srcs = new List<AudioObject>();
    [SerializeField] private AudioObject srcPrefab;
    [SerializeField] private int preSpawned;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }

    // Start is called before the first frame update
    void Start()
    {
        Prespawn();
    }

    private void Prespawn()
    {
        for (int i = 0; i < preSpawned; i++)
        {
            AudioObject src = Instantiate(srcPrefab);
            srcs.Add(src);
            src.gameObject.SetActive(false);
        }
    }

    private AudioObject Spawn(Transform reference)
    {
        for (int i = 0; i < srcs.Count; i++)
        {
            if (srcs[i].gameObject.activeInHierarchy) continue;

            srcs[i].transform.position = reference.position;
            srcs[i].gameObject.SetActive(true);
            return srcs[i];
        }

        AudioObject src = Instantiate(srcPrefab);
        srcs.Add(src);        
        return src;
    }

    public void PlayOneShot(Transform reference, AudioClip clip)
    {
        AudioObject src = Spawn(reference);
        src.PlayOneShot(clip);
    }

    public void Restart()
    {
        srcs.Clear();
        Prespawn();
    }
}
