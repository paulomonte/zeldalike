using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioObject : MonoBehaviour
{
    [SerializeField] private AudioSource src;
    private bool playingOneShot;

    // Update is called once per frame
    void Update()
    {
        if (src.time >= src.clip.length * 0.97f && playingOneShot)
        {
            playingOneShot = false;
            src.clip = null;
            gameObject.SetActive(false);
        }
    }

    public void PlayOneShot(AudioClip clip)
    {
        playingOneShot = true;
        src.clip = clip;
        src.Play();
    }
}
