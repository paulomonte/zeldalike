using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CameraManager : BaseManager<CinemachineVirtualCamera>
{
    public void ActivateCamera(CinemachineVirtualCamera camera)
    {
        for (int i = 0; i < items.Count; i++)
        {
            if (camera == items[i])
            {
                items[i].gameObject.SetActive(true);
                continue;
            }

            items[i].gameObject.SetActive(false);
        }
    }
}
