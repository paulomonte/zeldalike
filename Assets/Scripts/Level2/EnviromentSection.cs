using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class EnviromentSection : MonoBehaviour
{
    [SerializeField] private CameraManager cameraManager;
    [SerializeField] private CinemachineVirtualCamera sectionCamera;

    // Start is called before the first frame update
    void Start()
    {
        cameraManager.RegisterItem(sectionCamera);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            cameraManager.ActivateCamera(sectionCamera);
        }
    }
}
