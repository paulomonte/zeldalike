using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface Interactable
{
    bool Interactable { get; }
    string InteractionMessage { get; }
    void Interact(Component interactor);
    void ExitInteraction();
}
