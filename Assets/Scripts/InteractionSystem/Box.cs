using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Box : MonoBehaviour, Interactable
{    public enum BoxState
    {
        Free,
        Held,
        Target
    }

    [SerializeField] private BoxManager boxManager;
    [SerializeField] private Rigidbody rb;
    [SerializeField] private string interactionMessage;
    [SerializeField] private BoxState state;
    private Rigidbody interactorRb;
    private bool interactable = true;

    public bool Interactable { get => interactable; }
    public string InteractionMessage => interactionMessage;

    [SerializeField] private AudioSource src;

    private void Start()
    {
        boxManager.RegisterItem(this);
    }

    public void Interact(Component interactor)
    {
        if (!interactable)
            return;

        state = BoxState.Held;
        interactorRb = interactor as Rigidbody;
        rb.constraints = RigidbodyConstraints.None;
        rb.constraints = RigidbodyConstraints.FreezeRotation;
    }

    public void ExitInteraction()
    {
        if (!interactable)
            return;

        state = BoxState.Free;
        rb.velocity = Vector3.zero;
        interactorRb = null;
        rb.constraints = RigidbodyConstraints.None;
        rb.constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ;
    }

    private void OnReachTargetPosition()
    {
        interactable = false;
        state = BoxState.Target;
        rb.velocity = Vector3.zero;
        rb.constraints = RigidbodyConstraints.None;
        rb.constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ;
        print("end");
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (rb.velocity.magnitude > 0 && !src.isPlaying)
            src.Play();
        else if(src.isPlaying)
            src.Stop();

        if (state == BoxState.Free || state == BoxState.Target)
        {
            return;
        }

        Vector3 currentVelocity = interactorRb.velocity;
        currentVelocity.y = rb.velocity.y;
        rb.velocity = currentVelocity;
    }

    private void OnCollisionExit(Collision collision)
    {
        if(collision.gameObject.CompareTag("Floor"))
        {
            OnReachTargetPosition();
        }
    }
}
