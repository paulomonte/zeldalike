using System;
using System.Collections;
using System.Collections.Generic;
using Utils.CustomDelegates;
using UnityEngine;

public class Interactor : MonoBehaviour
{
    [SerializeField] private Transform raycastOrigin;
    [SerializeField] private LayerMask interactableLayer;
    [SerializeField] private float interactionMaxDistance = 2f;
    private Vector3 interactablePosition;
    private Interactable currentInteractable;
    private Interactable interactableWithinReach;

    public event InteractionHandler OnInteract;
    public event Action OnInteractionExit;
    public event InteractionMessageHandler OnAim;
    public event Action OnExitAim;

    // Start is called before the first frame update
    void Start()
    {
        var playerController = GetComponent<PlayerController>();
        playerController.OnInteractAction += PlayerController_OnPressInteract;
        playerController.OnInteractionExit += PlayerController_OnInteractionExit;
    }

    private void PlayerController_OnPressInteract(Component interactor)
    {
        if (currentInteractable == null)
            return;

        currentInteractable.Interact(interactor);
        OnInteract?.Invoke(interactablePosition, interactor);
    }

    private void PlayerController_OnInteractionExit()
    {
        if(currentInteractable == null)
            return;

        currentInteractable.ExitInteraction();
        OnInteractionExit?.Invoke();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Aim();
    }

    public void Aim()
    {
        currentInteractable = interactableWithinReach;
        RaycastHit hit;

        if (Physics.Raycast(raycastOrigin.position, raycastOrigin.forward, out hit, interactionMaxDistance, interactableLayer))
        {
            interactableWithinReach = hit.collider.GetComponent<Interactable>();
            interactablePosition = hit.normal;

            if (currentInteractable == interactableWithinReach)
                return;

            OnAim?.Invoke(interactableWithinReach.InteractionMessage);
        }
        else
        {
            if(currentInteractable == null)
            {
                return;
            }

            OnExitAim?.Invoke();
            OnInteractionExit?.Invoke();

            currentInteractable.ExitInteraction();
            currentInteractable = null;
            interactableWithinReach = null;
        }
    }
}
