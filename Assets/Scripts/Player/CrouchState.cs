﻿using UnityEngine;

public class CrouchState : PlayerMovementState
{
    public override int AnimationLayer => 1;

    public CrouchState(Rigidbody rb, Transform modelTransform, float moveSpeed, float rotationSpeed)
    {
        this.rb = rb;
        this.modelTransform = modelTransform;
        this.moveSpeed = moveSpeed;
        this.rotationSpeed = rotationSpeed;
        this.jumpForce = 0;
    }

    public override void Jump()
    {
#if UNITY_EDITOR
        Debug.Log("You cannot jump in this state");
#endif
    }

    public override void Rotate(float xValue)
    {
        base.Rotate(xValue);
        rb.transform.Rotate(0, xValue * rotationSpeed, 0);
    }
}
