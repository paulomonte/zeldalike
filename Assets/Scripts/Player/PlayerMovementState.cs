﻿using UnityEngine;

public abstract class PlayerMovementState : IMovementState
{
    protected Rigidbody rb;
    protected Transform modelTransform;
    protected float moveSpeed;
    protected float rotationSpeed;
    protected float jumpForce;
    public abstract int AnimationLayer { get; }
    public abstract void Jump();
    public virtual void Rotate(float xValue)
    {
        Vector3 finalDirection = new Vector3(rb.velocity.x, 0, rb.velocity.z).normalized;

        if (finalDirection != Vector3.zero)
            modelTransform.forward = finalDirection;
    }

    public virtual Vector3 Move(Transform reference, float inputX, float inputY)
    {
        Vector3 forwardSpeed = reference.forward * inputY * moveSpeed;
        Vector3 sideSpeed = reference.right * inputX * moveSpeed;
        Vector3 finalVelocity = forwardSpeed + sideSpeed;
        finalVelocity.y = rb.velocity.y;
        rb.velocity = finalVelocity;
        return finalVelocity;
    }
}
