using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAudioPlayer : MonoBehaviour
{
    [SerializeField] private PlayerController controller;
    [SerializeField] private AudioSource src;
    [SerializeField] private AudioClip yell;
    private int currentAnimationLayer;

    // Start is called before the first frame update
    void Start()
    {
        controller.OnLose += Controller_OnFound;
        controller.OnFootstep += Controller_OnFootstep;

        var playerAnimation = GetComponent<PlayerAnimationController>();
        playerAnimation.OnChangeLayer += PlayerAnimation_OnChangeLayer;
    }

    private void PlayerAnimation_OnChangeLayer(int layer)
    {
        currentAnimationLayer = layer;
    }

    private void Controller_OnFootstep()
    {
        if (currentAnimationLayer != 0)
            return;

        src.Play();
    }

    private void Controller_OnFound()
    {
        src.PlayOneShot(yell);
    }
}
