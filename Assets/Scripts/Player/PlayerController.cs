using System;
using System.Collections;
using System.Collections.Generic;
using Utils.CustomDelegates;
using UnityEngine;

public class PlayerController : BaseCharacterController<PlayerInputBase>
{
    private bool controlBlocked;
    private Camera cam;

    public event InteractionInputHandler OnInteractAction;
    public event Action OnInteractionExit;
    public event Action OnJump;
    public event Action OnCrouch;
    public event Action OnStandUp;

    public event Action OnLose;
    public event Action OnFootstep;

    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
        cam = Camera.main;
        OnLose += gameController.LoseGame;
    }

    protected override void Update()
    {
        HandleMovementInput(cam.transform);
        HandleActionInput();
    }

    protected override void HandleMovementInput(Transform inputReference)
    {
        if (controlBlocked)
        {
            ResetInputs(inputReference);
            return;
        }

        base.HandleMovementInput(inputReference);
    }

    protected override void HandleActionInput()
    {
        if (input.Pause)
        {
            gameController.AlternatePauseState();
        }

        if (gameController.gamePaused || controlBlocked)
            return;

        if (input.InteractPressed)
        {
            OnInteractAction?.Invoke(rb);
        }

        if(input.InteractReleased)
        {
            OnInteractionExit?.Invoke();
        }

        if (input.Jump)
        {
            OnJump?.Invoke();
        }

        if (input.CrouchPressed)
        {
            OnCrouch?.Invoke();
        }

        if (input.CrouchReleased)
        {
            OnStandUp?.Invoke();
        }
    }

    public void Found()
    {
        LoseGame();
    }

    private void LoseGame()
    {
        controlBlocked = true;
        OnLose?.Invoke();
        GameController.instance.LoseGame();
    }

    public void PlayFootsteps()
    {
        OnFootstep?.Invoke();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == 4)//Water layer
        {
            LoseGame();
        }
    }
}
