﻿using UnityEngine;

public class FreeState : PlayerMovementState
{
    public override int AnimationLayer => 0;

    public FreeState(Rigidbody rb, Transform modelTransform, float moveSpeed, float rotationSpeed, float jumpForce)
    {
        this.rb = rb;
        this.modelTransform = modelTransform;
        this.moveSpeed = moveSpeed;
        this.rotationSpeed = rotationSpeed;
        this.jumpForce = jumpForce;
    }

    public override void Jump()
    {
        Vector3 vel = rb.velocity;
        vel.y = 0;
        rb.velocity = vel;
        rb.AddForce(new Vector3(0, jumpForce, 0));
    }

    public override void Rotate(float xValue)
    {
        base.Rotate(xValue);
        rb.transform.Rotate(0, xValue * rotationSpeed, 0);
    }
}
