﻿public class PlayerInputBase : InputBase
{
    public bool Jump { get; protected set; }
    public bool CrouchPressed { get; protected set; }
    public bool CrouchReleased { get; protected set; }
    public bool InteractPressed { get; protected set; }
    public bool InteractReleased { get; protected set; }
    public bool Pause { get; protected set; }
}
