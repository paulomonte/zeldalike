using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputKeyboardAndMouse : PlayerInputBase
{
    [SerializeField] private KeyCode crouchKey;
    [SerializeField] private KeyCode pauseKey;

    // Update is called once per frame
    void Update()
    {
        MovementX = Input.GetAxis("Horizontal");
        MovementY = Input.GetAxis("Vertical");
        Jump = Input.GetKeyDown(KeyCode.Space);
        CrouchPressed = Input.GetKeyDown(crouchKey);
        CrouchReleased = Input.GetKeyUp(crouchKey);
        InteractPressed = Input.GetMouseButtonDown(0);
        InteractReleased = Input.GetMouseButtonUp(0);
        RotationX = Input.GetAxis("Mouse X");
        RotationY = Input.GetAxis("Mouse Y");
        Pause = Input.GetKeyDown(pauseKey);
    }
}
