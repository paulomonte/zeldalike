﻿using UnityEngine;

public interface IMovementState
{
    int AnimationLayer { get; }
    Vector3 Move(Transform reference, float inputX, float inputY);
    void Rotate(float xValue);
    void Jump();
}
