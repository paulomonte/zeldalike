﻿using UnityEngine;

public class InputBase : MonoBehaviour
{
    public float MovementX { get; protected set; }
    public float MovementY { get; protected set; }
    
    public float RotationX { get; protected set; }
    public float RotationY { get; protected set; }    
}
