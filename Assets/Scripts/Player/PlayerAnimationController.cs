using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimationController : MonoBehaviour
{
    [SerializeField] private Animator anim;
    private readonly string groundedParam = "grounded";
    private readonly string speedParam = "speed";
    private int layerInUse = 0;

    public event Action<int> OnChangeLayer;

    // Start is called before the first frame update
    void Start()
    {
        var playerMovement = GetComponent<PlayerMovement>();
        playerMovement.OnMove += PlayerMovement_OnMove;
        playerMovement.OnGroundCheck += PlayerMovement_OnGroundCheck;
        playerMovement.OnChangeState += PlayerMovement_OnChangeState;
    }

    private void PlayerMovement_OnChangeState(int layer)
    {
        UseLayer(layer);        
    }

    private void PlayerMovement_OnMove(float speed, float inputY)
    {
        SetSpeed(speed, inputY);
    }

    private void PlayerMovement_OnGroundCheck(bool isGrounded)
    {
        SetGrounded(isGrounded);
    }

    public void SetGrounded(bool value)
    {
        anim.SetBool(groundedParam, value);
    }

    public void SetSpeed(float speed, float inputY)
    {
        float speedValue = layerInUse == 2 ? inputY : speed;
        anim.SetFloat(speedParam, speedValue);
    }

    public void UseLayer(int layer)
    {
        StopAllCoroutines();
        layerInUse = layer;

        for (int i = 0; i < anim.layerCount; i++)
        {
            if (i == 0)
                continue;

            if (layer == i)
            {
                StartCoroutine(ChangeLayerWeight_Routine(i, anim.GetLayerWeight(i), 1));
                continue;
            }

            StartCoroutine(ChangeLayerWeight_Routine(i, anim.GetLayerWeight(i), 0));
        }

        OnChangeLayer?.Invoke(layer);
    }

    private IEnumerator ChangeLayerWeight_Routine(int layer, float from, float to)
    {        
        float rate = 0;
        float weight = from;

        while(anim.GetLayerWeight(layer) != to)
        {
            rate += Time.deltaTime / 0.15f;
            weight = Mathf.Lerp(from, to, rate);
            anim.SetLayerWeight(layer, weight);
            yield return null;
        }

        anim.SetLayerWeight(layer, to);
    }
}
