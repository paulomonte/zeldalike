﻿using UnityEngine;

public class HoldingState : PlayerMovementState
{
    public override int AnimationLayer => 2;

    public HoldingState(Rigidbody rb, Transform modelTransform, float moveSpeed)
    {
        this.rb = rb;
        this.modelTransform = modelTransform;
        this.moveSpeed = moveSpeed;
        this.rotationSpeed = 0;
        this.jumpForce = 0;
    }

    public override void Jump()
    {
#if UNITY_EDITOR
        Debug.Log("You cannot jump in this state");
#endif
    }

    public override void Rotate(float xValue)
    {
#if UNITY_EDITOR
        Debug.Log("You cannot rotate in this state");
#endif
    }

    public override Vector3 Move(Transform reference, float inputX, float inputY)
    {
        Vector3 forwardSpeed = reference.forward * inputY * moveSpeed;
        Vector3 finalVelocity = forwardSpeed;
        finalVelocity.y = rb.velocity.y;
        rb.velocity = finalVelocity;
        return finalVelocity;
    }
}