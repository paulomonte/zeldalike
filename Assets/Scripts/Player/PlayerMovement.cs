using System.Collections;
using System.Collections.Generic;
using Utils.CustomDelegates;
using UnityEngine;
using System;

[RequireComponent(typeof(PlayerController))]
public class PlayerMovement : MonoBehaviour
{
    [SerializeField] private Transform playerModel;
    private Rigidbody rb;
    [field: SerializeField] public bool isGrounded { get; private set; }


    private IMovementState state;
    [SerializeField] private float crouchMoveSpeed;
    [SerializeField] private float holdingMoveSpeed;
    [SerializeField] private float freeMoveSpeed;
    [SerializeField] private float freeRotationSpeed;
    [SerializeField] private int jumpForce;
    public bool UseRotation { get; set; }
    Vector3 finalVelocity;

    [SerializeField] private Vector3 checkBoxCenter;
    [SerializeField] private Vector3 checkBoxExtends;
    [SerializeField] private LayerMask groundLayer;

    public event MoveHandler OnMove;
    public event GroundCheckHandler OnGroundCheck;
    public event Action<int> OnChangeState;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        StartFreeState();
        SubscribeInPlayerControllerEvents();
        SubscribeInInteractorEvents();
    }

    private void SubscribeInPlayerControllerEvents()
    {
        var playerController = GetComponent<PlayerController>();
        playerController.OnMoveInput += PlayerController_OnMoveInput;
        playerController.OnRotateInput += PlayerController_OnRotateInput;
        playerController.OnJump += PlayerPhysicsHandler_OnJump;
        playerController.OnCrouch += PlayerController_OnCrouch;
        playerController.OnStandUp += PlayerController_OnStandUp;
    }

    private void PlayerController_OnStandUp()
    {
        if (state.AnimationLayer == 1)
            StartFreeState();
    }

    private void PlayerController_OnCrouch()
    {
        if (state.AnimationLayer == 0)
        {
            state = new CrouchState(rb, playerModel, crouchMoveSpeed, freeRotationSpeed);
            OnChangeState?.Invoke(state.AnimationLayer);
        }
    }

    private void SubscribeInInteractorEvents()
    {
        var interactor = GetComponent<Interactor>();

        if (!interactor)
            return;

        interactor.OnInteract += Interactor_OnInteract;
        interactor.OnInteractionExit += Interactor_OnInteractionExit;
    }

    private void Interactor_OnInteractionExit()
    {
        StartFreeState();
    }

    private void StartFreeState()
    {
        state = new FreeState(rb, playerModel, freeMoveSpeed, freeRotationSpeed, jumpForce);
        OnChangeState?.Invoke(state.AnimationLayer);
    }

    private void Interactor_OnInteract(Vector3 interactableNormal, Component interactor)
    {
        interactableNormal.y = 0;
        transform.forward = -interactableNormal;
        playerModel.localRotation = Quaternion.identity;
        state = new HoldingState(rb, playerModel, holdingMoveSpeed);
        OnChangeState?.Invoke(state.AnimationLayer);
    }

    private void PlayerController_OnMoveInput(Transform transform, float inputX, float inputY)
    {
        Move(transform, inputX, inputY);
    }

    private void PlayerController_OnRotateInput(float rotationInputX)
    {
        if (!UseRotation)
            rotationInputX = 0;

        Rotate(rotationInputX);
    }

    private void PlayerPhysicsHandler_OnJump()
    {
        Jump();
    }

    // Update is called once per frame
    void Update()
    {
        GroundCheck();
    }

    private void GroundCheck()
    {
        checkBoxCenter = transform.position;
        isGrounded = Physics.CheckBox(checkBoxCenter, checkBoxExtends, Quaternion.identity, groundLayer);
        OnGroundCheck?.Invoke(isGrounded);
    }

    private void Move(Transform reference, float inputX, float inputY)
    {
        finalVelocity = state.Move(reference, inputX, inputY);
        finalVelocity.y = 0;
        OnMove?.Invoke(finalVelocity.magnitude, inputY);
    }

    private void Rotate(float xValue)
    {
        state.Rotate(xValue);
    }

    public void Jump()
    {
        if (!isGrounded)
            return;

        state.Jump();
    }
}
