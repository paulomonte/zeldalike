using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGoal : MonoBehaviour
{
    [SerializeField] private string nextLevel;

    public event Action OnReachGoal;

    private void WinGame()
    {
        OnReachGoal?.Invoke();
        GameController.instance.OnReachGoal(nextLevel);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            WinGame();
        }
    }
}
