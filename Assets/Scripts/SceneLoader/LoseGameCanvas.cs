using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoseGameCanvas : MonoBehaviour
{
    [field: SerializeField] public float canvasDuration { get; private set; }

    public event Action OnEndCanvasDuration;

    private void Start()
    {
        GameController.instance.RegisterLoseCanvas(this);
        gameObject.SetActive(false);
    }

    public IEnumerator LosingGame_Routine()
    {
        yield return new WaitForSeconds(canvasDuration);
        OnEndCanvasDuration?.Invoke();
    }

    private void OnDestroy()
    {
        GameController.instance.DeregisterLoseCanvas();
    }
}
