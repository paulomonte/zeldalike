﻿using UnityEngine;

public class RotatingState : GuardState
{
    public RotatingState(Rigidbody rb, Transform target, float rotationDuration)
    {
        label = StateLabel.Rotating;
        this.rb = rb;
        this.target = target;
        this.moveSpeed = 0;
        this.rotationDuration = rotationDuration;
        lerpValue = 0;
    }
}

