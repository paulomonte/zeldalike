using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAnimatorController : MonoBehaviour
{
    [SerializeField] private RuntimeAnimatorController animator;
    [SerializeField] private Animator anim;
    private readonly string foundParam = "found";
    private readonly string speedParam = "speed";
    private readonly string lookingParam = "looking";

    // Start is called before the first frame update
    void Start()
    {
        var enemyMovement = GetComponent<EnemyMovement>();
        enemyMovement.OnMove += EnemyMovement_OnMove;
        enemyMovement.OnChangeLookingState += EnemyMovement_OnChangeLookingState;

        var sightController = GetComponentInChildren<SightController>();
        sightController.OnPlayerInSight += SightController_OnPlayerInSight;
    }

    public void FillAnimator(Animator model)
    {
        anim = model;

        if (anim.runtimeAnimatorController == null)
            anim.runtimeAnimatorController = animator;
    }

    private void SightController_OnPlayerInSight(Transform player)
    {
        anim.SetTrigger(foundParam);
    }

    private void EnemyMovement_OnChangeLookingState(bool isLooking)
    {
        anim.SetBool(lookingParam, isLooking);
    }

    private void EnemyMovement_OnMove(float speed, float inputY)
    {
        SetSpeed(speed);
    }

    public void SetSpeed(float value)
    {
        anim.SetFloat(speedParam, value);
        anim.SetFloat(speedParam, value);
    }
}
