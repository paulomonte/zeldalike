﻿using UnityEngine;

public class WalkingState : GuardState
{
    public WalkingState(Rigidbody rb, Transform target, float moveSpeed)
    {
        label = StateLabel.Walking;
        this.rb = rb;
        this.target = target;
        this.moveSpeed = moveSpeed;
        this.rotationDuration = 0;
        lerpValue = 0;
    }
}

