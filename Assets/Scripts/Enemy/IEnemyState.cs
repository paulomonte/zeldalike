﻿using UnityEngine;

public interface IEnemyState
{
    GuardState.StateLabel Label { get; }

    Vector3 Move(Transform reference, float inputX);
    (Quaternion currentRotation, float rotationLerpValue) Rotate(Transform reference, float xValue);
}

