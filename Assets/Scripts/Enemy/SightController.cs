using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SightController : MonoBehaviour
{
    private PlayerController playerInRange;
    [SerializeField] private float sightDistance;
    private bool foundPlayer;

    public event Action<Transform> OnPlayerInSight;

    private void Start()
    {
        Vector3 targetPos = transform.localPosition;
        targetPos.z = sightDistance / 2;
        transform.localPosition = targetPos;
        transform.localScale = Vector3.one * sightDistance;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (playerInRange == null)
            return;

        if (CheckPlayerInSight() && !foundPlayer)
        {
            foundPlayer = true;
            playerInRange.Found();
            OnPlayerInSight?.Invoke(playerInRange.transform);
        }
    }

    private bool CheckPlayerInSight()
    {
        Vector3 origin = transform.parent.position;
        origin.y = transform.position.y;
        Vector3 direction = playerInRange.transform.position - transform.parent.position;        
        RaycastHit hit;

        Debug.DrawLine(origin, direction, Color.green, 1);
        if (Physics.Raycast(origin, direction, out hit, sightDistance))
        {
            return hit.collider.CompareTag("Player");
        }
        else
        {
            return false;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            playerInRange = other.GetComponent<PlayerController>();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            playerInRange = null;
        }
    }
}
