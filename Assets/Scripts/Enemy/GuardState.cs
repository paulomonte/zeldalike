﻿using UnityEngine;

public class GuardState : IEnemyState
{
    public enum StateLabel
    {
        Walking,
        Looking,
        Rotating,
        Found
    }

    protected StateLabel label;
    public StateLabel Label { get => label; }
    protected Rigidbody rb;
    protected Transform target;
    protected float moveSpeed;
    protected float rotationDuration;
    public float lerpValue;

    public virtual Vector3 Move(Transform reference, float inputX)
    {
        Vector3 finalVelocity = reference.forward * inputX * moveSpeed;
        finalVelocity.y = 0;
        rb.velocity = finalVelocity;
        return finalVelocity;
    }

    public virtual (Quaternion currentRotation, float rotationLerpValue) Rotate(Transform reference, float xValue)
    {
        Quaternion startRotation = reference.rotation;
        Vector3 targetDirection = target.position - reference.position;
        Quaternion targetRotation = Quaternion.LookRotation(targetDirection);

        if (lerpValue < 1)
            lerpValue += (Time.deltaTime / rotationDuration);
                
        Quaternion currentRotation = Quaternion.Lerp(startRotation, targetRotation, lerpValue);
        return (currentRotation, lerpValue);
    }
}

