using System.Collections;
using System.Collections.Generic;
using Utils.CustomDelegates;
using UnityEngine;
using System;

public class EnemyMovement : MonoBehaviour
{
    private Rigidbody rb;

    private IEnemyState state;
    [SerializeField] private List<Transform> targets;
    [SerializeField] private int currentTarget = 0;
    [SerializeField] private float moveSpeed;
    [SerializeField] private float rotationDuration;
    [SerializeField] private float lookStateDuration;
    private Vector3 finalVelocity;

    public event MoveHandler OnMove;
    public event Action<Transform> OnEndRotation;
    public event Action<bool> OnChangeLookingState;


    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        OnEndRotation += StartNewWalkingState;
        state = new RotatingState(rb, targets[currentTarget], rotationDuration);

        var enemyController = GetComponent<EnemyController>();
        enemyController.OnMoveInput += EnemyController_OnMoveInput;
        enemyController.OnRotateInput += EnemyController_OnRotateInput;

        var sightController = GetComponentInChildren<SightController>();
        sightController.OnPlayerInSight += SightController_OnPlayerInSight;
    }

    private void SightController_OnPlayerInSight(Transform player)
    {
        targets.Add(player);
        currentTarget = targets.Count - 1;
        StopAllCoroutines();
        OnChangeLookingState?.Invoke(false);
        OnEndRotation += StartNewLookingState;
        state = new RotatingState(rb, targets[currentTarget], rotationDuration);
    }

    private void EnemyController_OnRotateInput(float rotationInputX)
    {
        Rotate(rotationInputX);
    }

    private void EnemyController_OnMoveInput(Transform transform, float inputX, float inputY)
    {
        Move(transform, inputX, inputY);
        float distanceFromTarget = Vector3.Distance(transform.position, targets[currentTarget].position);
        
        if(distanceFromTarget <= 0.05f && state.Label == GuardState.StateLabel.Walking)
        {
            StartNewLookingState();
        }
    }

    private void StartNewLookingState()
    {
        currentTarget++;

        if (currentTarget > targets.Count - 1)
            currentTarget = 0;

        state = new LookingState(rb, targets[currentTarget]);
        StartCoroutine(LookingStateCountDown());
    }

    private IEnumerator LookingStateCountDown()
    {
        OnChangeLookingState?.Invoke(true);
        float currentValue = lookStateDuration;

        while(currentValue > 0)
        {
            currentValue -= Time.deltaTime;
            yield return null;
        }

        OnEndRotation += StartNewWalkingState;
        state = new RotatingState(rb, targets[currentTarget], rotationDuration);
        OnChangeLookingState?.Invoke(false);
    }

    private void Move(Transform reference, float inputX, float inputY)
    {
        finalVelocity = state.Move(reference, inputX);
        OnMove?.Invoke(finalVelocity.magnitude, 0);
    }

    private void Rotate(float xValue)
    {
        if (state.Label != GuardState.StateLabel.Rotating)
            return;

        (Quaternion newRotation, float lerp) = state.Rotate(transform, xValue);
        transform.rotation = newRotation;

        if(lerp >= 1)
        {
            OnEndRotation?.Invoke(targets[currentTarget]);
            OnEndRotation = null;
        }
    }

    private void StartNewWalkingState(Transform target)
    {
        state = new WalkingState(rb, target, moveSpeed);

    }

    private void StartNewLookingState(Transform target)
    {
        state = new LookingState(rb, target);
    }
}
