﻿using UnityEngine;

public class LookingState : GuardState
{
    public LookingState(Rigidbody rb, Transform target)
    {
        label = StateLabel.Looking;
        this.rb = rb;
        this.target = target;
        this.moveSpeed = 0;
        this.rotationDuration = 0;
        lerpValue = 0;
    }
}

