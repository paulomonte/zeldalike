﻿using Utils.CustomDelegates;
using UnityEngine;

public class BaseCharacterController<T> : MonoBehaviour where T : InputBase
{
    protected GameController gameController;
    protected T input;
    protected Rigidbody rb;

    public event MovementInputHandler OnMoveInput;
    public event RotationInputHandler OnRotateInput;

    // Start is called before the first frame update
    protected virtual void Start()
    {
        gameController = GameController.instance;
        rb = GetComponent<Rigidbody>();
        input = GetComponent<T>();
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        HandleMovementInput(transform);
        HandleActionInput();
    }

    protected virtual void HandleMovementInput(Transform inputReference)
    {
        if (gameController.gamePaused)
            return;

        OnMoveInput?.Invoke(inputReference, input.MovementX, input.MovementY);
        OnRotateInput?.Invoke(input.RotationX);
    }

    protected void ResetInputs(Transform inputReference)
    {
        OnMoveInput?.Invoke(inputReference, 0, 0);
        OnRotateInput?.Invoke(0);
    }

    protected virtual void HandleActionInput()
    {
        if (gameController.gamePaused)
            return;
    }
}
