using System.Collections;
using System.Collections.Generic;
using Utils.CustomDelegates;
using UnityEngine;

public class EnemyController : BaseCharacterController<EnemyInput>
{
    [Tooltip("In case of model null, this one will be used")]
    [SerializeField] private GameObject defaultModel;
    [Tooltip("If you wish chose a model, placee it here")]
    [SerializeField] private Animator model;


    public event InteractionInputHandler OnInteractAction;

    // Start is called before the first frame update
    protected override void Start()
    {
        if (model)
        {
            if (defaultModel)
                Destroy(defaultModel);

            Animator newModel = Instantiate(model, transform.position, transform.rotation, transform);
            GetComponent<EnemyAnimatorController>().FillAnimator(newModel);
        }

        base.Start();
    }


}

