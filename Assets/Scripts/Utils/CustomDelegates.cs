using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Utils.CustomDelegates
{
    public delegate void MovementInputHandler(Transform transform, float inputX, float inputY);
    public delegate void RotationInputHandler(float rotationInputX);
    public delegate void InteractionInputHandler(Component component);

    public delegate void MoveHandler(float speed, float inputY);
    public delegate void GroundCheckHandler(bool isGrounded);

    public delegate void InteractionHandler(Vector3 interactionPoint, Component interactor);
    public delegate void InteractionMessageHandler(string message);
}
