using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxManager : BaseManager<Box>
{
    public List<Vector3> positions = new List<Vector3>();
    public List<Vector3> Positions { get => positions; }


    private void Awake()
    {
        BoxesData data = SaverSystem.Load<BoxesData>("boxesPosition");

        if (data == null || data.boxesPositions == null || data.boxesPositions.Count == 0)
        {
            return;
        }

        for (int i = 0; i < data.boxesPositions.Count; i++)
        {
            Vector3 position = data.boxesPositions[i].ToVector3();
            positions.Add(position);
        }
    }

    public override void RegisterItem(Box item)
    {
        base.RegisterItem(item);

        int lastIndex = items.Count - 1;

        if (positions.Count > 0)
            item.transform.position = positions[lastIndex];
    }
}
