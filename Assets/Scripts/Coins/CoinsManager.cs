using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinsManager : BaseManager<Coin>
{
    [SerializeField] private int collected;

    public event Action<int> OnUpdateCollectedAmount;

    public override void RegisterItem(Coin item)
    {
        base.RegisterItem(item);
        item.OnCollect += Coin_OnCollect;
    }

    private void Coin_OnCollect(Coin coin)
    {
        collected += coin.value;
        OnUpdateCollectedAmount?.Invoke(collected);
    }
}
