using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
    [SerializeField] private CoinsManager manager;
    [field: SerializeField] public int value { get; private set; }
    [field: SerializeField] public Color color { get; private set; }
    [SerializeField] private float rotateSpeed;
    [SerializeField] private AudioClip soundFX;

    public event Action<Coin> OnCollect;

    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Renderer>().material.color = color;
        manager.RegisterItem(this);
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(0, 0, rotateSpeed  * Time.deltaTime);
    }

    private void OnDisable()
    {
        manager.DeregisterItem(this);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            print("enter");
            OnCollect?.Invoke(this);
            AudioSystem.instance.PlayOneShot(transform, soundFX);
            gameObject.SetActive(false);
        }
    }
}
