﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class BoxesData
{
    public List<DataVector> boxesPositions = new List<DataVector>();
}

[Serializable]
public class DataVector
{
    public float x;
    public float y;
    public float z;

    public DataVector() { }

    public DataVector(Vector3 vector)
    {
        x = vector.x;
        y = vector.y;
        z = vector.z;
    }

    public Vector3 ToVector3()
    {
        return new Vector3(x, y, z);
    }
}