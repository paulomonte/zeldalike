using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveBoxesOnGoal : MonoBehaviour
{
    [SerializeField] private BoxManager boxesManager;

    // Start is called before the first frame update
    void Start()
    {
        var levelGoal = GetComponent<LevelGoal>();
        levelGoal.OnReachGoal += LevelGoal_OnReachGoal;
    }

    private void LevelGoal_OnReachGoal()
    {
        BoxesData data = new BoxesData();

        for (int i = 0; i < boxesManager.Items.Count; i++)
        {
            DataVector dataVector = new DataVector(boxesManager.Items[i].transform.position) ;
            data.boxesPositions.Add(dataVector);
        }

        SaverSystem.Save(data, "boxesPosition");
    }
}


