﻿using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEngine;

public static class SaverSystem
{
    public static void Save<T>(T data, string fileName)
    {
        string path = $"{new SaveLoadPathBuilder(fileName).Path}.bin";
        Debug.Log($"Save path: {path}");
        Debug.Log($"Save data: {data}");

        var binaryFormatter = new BinaryFormatter();
        var fileStream = new FileStream(path, FileMode.OpenOrCreate);

        binaryFormatter.Serialize(fileStream, data);
        fileStream.Close();
    }

    public static T Load<T>(string fileName)
    {
        string path = $"{new SaveLoadPathBuilder(fileName).Path}.bin";

        if (!File.Exists(path))
            return default(T);

        var binaryFormatter = new BinaryFormatter();
        var fileStream = new FileStream(path, FileMode.Open);

        T data = (T)binaryFormatter.Deserialize(fileStream);
        fileStream.Close();
        return data;
    }

    public static void Clear(string fileName)
    {
        string path = $"{new SaveLoadPathBuilder(fileName).Path}.bin";

        if(File.Exists(path))
        {
            File.Delete(path);
        }
    }
}
