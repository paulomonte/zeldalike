using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UiController : MonoBehaviour
{
    [Header("Interaction")]
    private Interactor interactor;
    [SerializeField] private Text interactionMessage;
    [SerializeField] private Image interactionIcon;
    [SerializeField] private float animationDuration = 0.5f;

    [Header("Coins")]
    [SerializeField] private CoinsManager coinManager;
    [SerializeField] private Text coinsCounter;

    // Start is called before the first frame update
    void Start()
    {
        interactor = FindObjectOfType<Interactor>();
        interactor.OnAim += Player_OnAim;
        interactor.OnExitAim += Interactor_OnExitAim;

        coinManager.OnUpdateCollectedAmount += CoinManager_OnUpdateCollectedAmount;
    }

    private void CoinManager_OnUpdateCollectedAmount(int amount)
    {
        coinsCounter.text = amount.ToString();
    }

    private void Interactor_OnExitAim()
    {
        StartCoroutine(CleanInteractionMessage());
    }

    private void Player_OnAim(string message)
    {
        if(string.IsNullOrEmpty(message))
        {
            StartCoroutine(CleanInteractionMessage());
            return;
        }

        StartCoroutine(ShowMessage(message));        
    }

    private IEnumerator ShowMessage(string message)
    {
        yield return ShrinkIconAnimation();

        interactionMessage.gameObject.SetActive(true);
        interactionMessage.text = message;

        yield return ExpandIconAnimation();
    }

    private IEnumerator CleanInteractionMessage()
    {
        yield return ShrinkIconAnimation();
        interactionMessage.gameObject.SetActive(false);
        yield return ExpandIconAnimation();
    }

    private IEnumerator ShrinkIconAnimation()
    {
        while (interactionIcon.transform.localScale.y > 0)
        {
            Vector3 scale = interactionIcon.transform.localScale;
            scale.y -= Time.deltaTime / animationDuration;
            interactionIcon.transform.localScale = scale;
            yield return null;
        }
    }

    private IEnumerator ExpandIconAnimation()
    {
        while (interactionIcon.transform.localScale.y < 1)
        {
            Vector3 scale = interactionIcon.transform.localScale;
            scale.y += Time.deltaTime / animationDuration;
            interactionIcon.transform.localScale = scale;
            yield return null;
        }
    }
}
