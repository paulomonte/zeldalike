using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Coin)), CanEditMultipleObjects]
public class CoinEditor : Editor
{
    private Coin coin;
    private Material coinMaterial;

    private void OnEnable()
    {
        if (coin == null)
        {
            coin = (Coin)target;
            coinMaterial = coin.GetComponent<Renderer>().sharedMaterial;
        }

        coinMaterial.color = coin.color;
    }

    private void OnValidate()
    {
        coinMaterial = coin.GetComponent<Renderer>().sharedMaterial;
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        coinMaterial.color = coin.color;
    }
}
